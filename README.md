PropCourier Sans
===============================

PropCourier Sans is a proportional version of OSP’s [NotCourier Sans](http://osp.kitchen/foundry/notcouriersans), forked by [Manufactura Independente](http://manufacturaindependente.org).

PropCourier Sans is the ‘official’ typeface of [Libre Graphics Magazine](http://libregraphicsmag.com). The typeface will be gradually improved and tweaked between issues.
The majority of the changes are in the left and right bearings of each glyph. Some glyphs have been changed to fit better into a proportional design (e.g. lowercase i). We have also improved kernings for the most commonly used words such as F/LOSS.

The original NotCourier Sans can be found at the [OSP Foundry](http://osp.kitchen/foundry).


Basic Font Information
----------------------

PropCourier Sans is available in three weights: Regular, Medium, Bold and Italic.

NotCourier was designed by OSP (Ludivine Loiseau), in July 2008. 
It is based on Nimbus Mono, copyright 1999 by (URW)++ Design & Development; Cyrillic glyphs added by Valek Filippov (C) 2001-2005.

Versions featured in different issues of Libre Graphics magazine can be found the [this](https://gitlab.com/libregraphicsmag/propcouriersans/) repository.

Licensing
---------

PropCourier Sans is distributed under the terms of the Open Font License (OFL).
See the full license at http://scripts.sil.org/OFL


ChangeLog
---------
2.2
   December 2013: added Italic variant.

2.1
	August 2012, Lurs: first attempt at producing a Medium weight.
	August 2012, Lurs: corrected the placement of diacritics for a few dozen of glyphs.

1.4
    April 2012, Porto: kerning tweaks.

1.3
    August 2011, Porto: automatic spacing adjustments using the Transpacing script,
    transplanting the Dejavu Sans spacings into PropCourier Sans.

1.2
    March 2011, Porto: punctuation tweaking to accomodate proportional spacing.

1.1
    November 2010, Porto: first release.


Contributors
------------

[Manufactura Independente](http://manufacturaindependente.org) (font maintainer)
Ana Isabel Carvalho & Ricardo Lafuente - [@ManufacturaInd](http://twitter.com/manufacturaind)
* Initial release
* Regular and bold weights
* Smaller details to accomodate proportional spacing
* Kerning pairs

Manuel Schmalstieg - [@greyscalepress](http://twitter.com/greyscalepress)
* Correction of diacritics
* Development of medium weight
